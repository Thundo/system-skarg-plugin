# System Skarg
System Skarg to plugin (wtyczka) do Google Chrome zmieniająca nazwy systemu zgłoszeń i rzeczy z tym
związanych

**Plugin jest jeszcze niedopracowany i jest w fazie BETA**

<p align="center">
  <img src="https://i.imgur.com/AbMbVYH.png">
</p>

## Instalacja

Proste. Po prostu:

* Pobierz repozytorium używając przycisku **Download**, a następnie wybierając jeden z formatów archiwum (np. **7ZIP**):

<p align="center">
  <img src="https://i.imgur.com/fsRfByP.png">
</p>

* **Rozpakuj archiwum.** 

* Uruchom Google Chrome i z listy po prawej wybierz **Więcej narzędzi > Rozszerzenia**:

<p align="center">
  <img src="https://i.imgur.com/xIRGvuS.png">
</p>

* W prawym górnym rogu przełącz **tryb dewelopera** (jeśli nie jest przełączony) i wybierz z pojawionego się menu **Załaduj rozpakowane**:

<p align="center">
  <img src="https://i.imgur.com/rtXoEmf.png">
</p>

* Przy pomocy eksploratora plików wybierz z wcześniej rozpakowanego archiwum folder **plugin**.

**Brawo plugin zainstalowany!**

## Sposób działania

Plugin automatycznie zaraz po wejściu na stronę https://td2.info.pl/index.php zacznie zmieniąć nazwy.

## BETA

Wtyczka ta została napisana w 30 minut i może zawierać **BŁĘDY**!

## Zakończenie

Teksty zawarte w tym prosty rozszerzeniu nie mają na celu nikogo urazić!
Czysto zabawowy plugin 😊

ver. 1.1
